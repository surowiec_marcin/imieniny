package vege.imieniny;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

//    @BindView(R.id.text_widget_data)TextView text_widget_data;
//    @BindView(R.id.text_widget_names)TextView text_widget_names;

    TextView text_widget_data;

    static SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
    static SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
    static SimpleDateFormat day_date = new SimpleDateFormat("dd");

    public static final String WIDGET_IDS_KEY ="new_app_widget";
    public static final String WIDGET_DATA_KEY ="new_app+widget_info";

    static Date d1 = new Date();
//    static Date d2 = new Date();
//    static Date d3 = new Date();
    static String dayOfTheWeek = sdf.format(d1);
    static String month=month_date.format(d1);
    static String day = day_date.format(d1);
    //String numberOfMonth = sdf.f

    static String newFormat= "dd.MM";
    static SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
    static String dateForSearch = timeFormat.format(d1);

    private static DatabaseHelper databaseHelper;
    protected static Context contextWidget;

    static List<Imiona> listOfImportantNames=new ArrayList<>();

//    String imieninyFirst=databaseHelper.getDateIstotne(dateForSearch).get(0).getName();


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(WIDGET_IDS_KEY)) {
            int[] ids = intent.getExtras().getIntArray(WIDGET_IDS_KEY);
            if (intent.hasExtra(WIDGET_DATA_KEY)) {
                Object data = intent.getExtras().getParcelable(WIDGET_DATA_KEY);
                this.update(context, AppWidgetManager.getInstance(context), ids, data);
            } else {
                this.onUpdate(context, AppWidgetManager.getInstance(context), ids);
            }
        } else super.onReceive(context, intent);
    }
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);
        views.setTextViewText(R.id.text_widget_data, dayOfTheWeek);
        views.setTextViewText(R.id.text_widget_data2, day);
        views.setTextViewText(R.id.text_data3, month);


        databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
        //databaseHelper.openDatabase();

        listOfImportantNames.addAll(databaseHelper.getDateIstotne(dateForSearch));
        String imiona1="";
        for(int i=0;i<listOfImportantNames.size();i++){
            imiona1=imiona1+", "+listOfImportantNames.get(i).getName();
        }
        imiona1=imiona1.substring(2);
        views.setTextViewText(R.id.textView8, imiona1);
//
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
        AppWidgetManager.getInstance(context).updateAppWidget(
                new ComponentName(context, NewAppWidget.class), views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
//        text_widget_data = (TextView) findViewById(R.id.text_widget_data);
//        text_widget_data = (TextView)rootView.findViewById(R.id.text_widget_data);
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
            databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
            databaseHelper.openDatabase();
            d1 = new Date();
            dayOfTheWeek = sdf.format(d1);
            month=month_date.format(d1);
            day = day_date.format(d1);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);


            views.setTextViewText(R.id.text_widget_data, dayOfTheWeek);
            views.setTextViewText(R.id.text_widget_data2, day);
            views.setTextViewText(R.id.text_data3, month);

            databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
            //databaseHelper.openDatabase();

            listOfImportantNames.addAll(databaseHelper.getDateIstotne(dateForSearch));
            String imiona1="";
            for(int i=0;i<listOfImportantNames.size();i++){
                imiona1=imiona1+", "+listOfImportantNames.get(i).getName();
            }

            imiona1=imiona1.substring(2);
            views.setTextViewText(R.id.textView8, imiona1);
            appWidgetManager.updateAppWidget(appWidgetId, views);
            AppWidgetManager.getInstance(context).updateAppWidget(
                    new ComponentName(context, NewAppWidget.class), views);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        d1 = new Date();
        dayOfTheWeek = sdf.format(d1);
        month=month_date.format(d1);
        day = day_date.format(d1);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
        //databaseHelper.openDatabase();

        views.setTextViewText(R.id.text_widget_data, dayOfTheWeek);
        views.setTextViewText(R.id.text_widget_data2, day);
        views.setTextViewText(R.id.text_data3, month);

        databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
        //databaseHelper.openDatabase();

        listOfImportantNames.addAll(databaseHelper.getDateIstotne(dateForSearch));
        String imiona1="";
        for(int i=0;i<listOfImportantNames.size();i++){
            imiona1=imiona1+", "+listOfImportantNames.get(i).getName();
        }

        imiona1=imiona1.substring(2);
        views.setTextViewText(R.id.textView8, imiona1);

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public static String changeLanguage(String str){
        String s="";
        switch (str){
            case "Monday":
                s="Poniedziałek";
                break;
            case "Tuesday":
                s="Wtorek";
                break;
            case "Thursday":
                s="Czwartek";
                break;
            case "Wednesday":
                s="Środa";
                break;
            case "Friday":
                s="Piątek";
                break;
            case "Saturday":
                s="Sobota";
                break;
            case "Sunday":
                s="Niedziela";
                break;
            default: s="xxx";
                break;
        }
        return s;
    }
    public void update(Context context, AppWidgetManager manager, int[] ids, Object data) {

        //data will contain some predetermined data, but it may be null
        for (int widgetId : ids) {

            d1 = new Date();
            dayOfTheWeek = sdf.format(d1);
            month=month_date.format(d1);
            day = day_date.format(d1);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
            //databaseHelper.openDatabase();

            views.setTextViewText(R.id.text_widget_data, dayOfTheWeek);
            views.setTextViewText(R.id.text_widget_data2, day);
            views.setTextViewText(R.id.text_data3, month);

            databaseHelper = new DatabaseHelper(DatabaseHelper.getmContext());
            //databaseHelper.openDatabase();

            listOfImportantNames.addAll(databaseHelper.getDateIstotne(dateForSearch));
            String imiona1="";
            for(int i=0;i<listOfImportantNames.size();i++){
                imiona1=imiona1+", "+listOfImportantNames.get(i).getName();
            }

            imiona1=imiona1.substring(2);
            views.setTextViewText(R.id.textView8, imiona1);
        }
    }
    public static void updateMyWidgets(Context context, Parcelable data) {
        AppWidgetManager man = AppWidgetManager.getInstance(context);
        int[] ids = man.getAppWidgetIds(
                new ComponentName(context,NewAppWidget.class));
        Intent updateIntent = new Intent();
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(NewAppWidget.WIDGET_IDS_KEY, ids);
        updateIntent.putExtra(NewAppWidget.WIDGET_DATA_KEY, data);
        context.sendBroadcast(updateIntent);
    }


}

