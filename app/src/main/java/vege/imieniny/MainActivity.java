package vege.imieniny;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private  DatabaseHelper databaseHelper;
    @BindView(R.id.textView1)TextView textView1;
    @BindView(R.id.fab)FloatingActionButton fab;

    protected static int width;
    protected static int height;

    List<Imiona> a;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Point size = new Point();
        width = size.x;
        height = size.y;

        startDatabase();
        String sb =databaseHelper.getThisDay("1.02").toString();
        String sb2 =(databaseHelper.getThisDayImportant("1.02").toString());
        sb=cutFirstAndLast(sb);
        sb2=cutFirstAndLast(sb2);
        //textView1.setText(sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb+sb2 +"\n Oraz \n"+sb);

        createDialogWithoutDateField().show();
        a=new ArrayList<>();

        a.addAll(databaseHelper.getSpecyficName("a"));
        textView1.setText(a.get(0).getName().toString());

    }

    public static String cutFirstAndLast(String s){
        StringBuilder sb = new StringBuilder(s);
        sb.deleteCharAt(0);
        sb.deleteCharAt(sb.length()-1);
        return String.valueOf(sb);
    }
    ////////////////////////////////////////////////////
    ////DataBase//////////
    ////////////////////////////////////////////////////

    public void startDatabase(){

        databaseHelper=new DatabaseHelper(this);
        File database=getBaseContext().getDatabasePath(DatabaseHelper.getDBNAME());

        if(false == database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if(copyDatabase(this)) {
                // Toast.makeText(this, "Za chwilę nas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Błąd kopiowania bazy danych", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }
    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[]buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity","DB copied");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /////////////Database end////////////////

    private DatePickerDialog createDialogWithoutDateField() {
        DatePickerDialog dpd = new DatePickerDialog(this, null, 2014, 1, 24);
        try {
            java.lang.reflect.Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
            for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField.get(dpd);
                    java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
                    for (java.lang.reflect.Field datePickerField : datePickerFields) {
                        Log.i("test", datePickerField.getName());
                        if ("mDaySpinner".equals(datePickerField.getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
        }
        return dpd;
    }
    @OnClick(R.id.fab)
    public void startAllNames(){
        Intent a = new Intent(MainActivity.this, AllNames.class);
        startActivity(a);
    }
}
