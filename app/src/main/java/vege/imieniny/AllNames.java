package vege.imieniny;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.widget.TextView;


import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllNames extends AppCompatActivity {

    private static DatabaseHelper databaseHelper;
    protected String choosenName;

    List<Imiona> imionaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_names);
        ButterKnife.bind(this);
        databaseHelper = new DatabaseHelper(this);
        databaseHelper.openDatabase();
        // tworzymy liste zawierajaca obiekty

        if(choosenName==null){
            choosenName="";}

        imionaList=new ArrayList<>();

        imionaList.addAll(databaseHelper.getSpecyficName(choosenName));



//         odnajdujemy recyclerview po id z XML
        final RecyclerView mojRecycler = (RecyclerView) findViewById(R.id.myRV);
        mojRecycler.addItemDecoration(new SimpleDividerItemDecoration(this));

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        // ustwaiamy nasz wlasny adapter do recyclerview
        AllNamesAdapter pa = new AllNamesAdapter(imionaList);
        mojRecycler.setAdapter(pa);



        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Rozpocznij wpisywanie imienia");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                choosenName=query;
                imionaList.clear();
                imionaList.addAll(databaseHelper.getSpecyficName(choosenName));
                AllNamesAdapter pa = new AllNamesAdapter(imionaList);
                mojRecycler.setAdapter(pa);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                choosenName=newText;
                imionaList.clear();
                imionaList.addAll(databaseHelper.getSpecyficName(choosenName));
                AllNamesAdapter pa = new AllNamesAdapter(imionaList);
                mojRecycler.setAdapter(pa);
                return false;
            }

    });
    }



}
