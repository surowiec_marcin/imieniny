package vege.imieniny;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vege on 19.08.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "imieniny.db";
    public static final String DBLOCATION = "/data/data/vege.imieniny/databases/";
    public static Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    public static String getDBNAME() {
        return DBNAME;
    }

    public static String getDBLOCATION() {
        return DBLOCATION;
    }

    public static Context getmContext() {
        return DatabaseHelper.mContext;
    }

    public SQLiteDatabase getmDatabase() {
        return mDatabase;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath="";
        try {
            dbPath = mContext.getDatabasePath(DBNAME).getPath();
        }
        catch (NullPointerException exc){
            mContext=NewAppWidget.contextWidget;
            dbPath = mContext.getDatabasePath(DBNAME).getPath();
        }
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }



    public List<Imiona> getNames() {
        Imiona imiona = null;
        List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM imiona", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imiona = new Imiona(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
            imionaList.add(imiona);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return imionaList;
    }

    public List<Imiona> getSpecyficName(String s) {
        Imiona imiona = null;
        List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM imiona WHERE imie LIKE '"+s+"%'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imiona = new Imiona(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
            imionaList.add(imiona);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return imionaList;
    }
    public List<Imiona> getDateIstotne(String s) {
        Imiona imiona = null;
        List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM imiona WHERE daty LIKE '%"+s+"%'  AND Istotne=1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imiona = new Imiona(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
            imionaList.add(imiona);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return imionaList;
    }
    public List<Imiona> getDateNieIstotne(String s) {
        Imiona imiona = null;
        List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM imiona WHERE daty LIKE '%"+s+"%' AND istotne=0", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imiona = new Imiona(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
            imionaList.add(imiona);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return imionaList;
    }

    public List<String> getThisDayImportant(String date) {
        String imie = null;
        List<String> namesForToday = new ArrayList<>();
        //List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT imie FROM imiona WHERE daty LIKE '%"+date+"%' and istotne=1;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imie = (cursor.getString(0));
            namesForToday.add(imie);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return namesForToday;
    }
    public List<String> getThisDay(String date) {
        String imie = null;
        List<String> namesForToday = new ArrayList<>();
        //List<Imiona> imionaList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT imie FROM imiona WHERE daty LIKE '%"+date+"%' and istotne=0 ;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            imie = (cursor.getString(0));
            namesForToday.add(imie);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return namesForToday;
    }
}
