package vege.imieniny;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.Normalizer;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Vege on 20.08.2017.
 */

public class AllNamesAdapter extends RecyclerView.Adapter<AllNamesAdapter.MyViewHolder>  {

    private List<Imiona> listOfNames;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_item_all_names,parent,false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // obiekt do ktorego jest przypisywany obiekt na odpowiedniej pozycji
        Imiona nameObject = listOfNames.get(position);
        holder.dates_view.setText(replaceChars(nameObject.getDate()));
        holder.name_view.setText(nameObject.getName());

    }

    @Override
    public int getItemCount() {
        return listOfNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView dates_view;
        public TextView name_view;

        public MyViewHolder(View itemView) {
            super(itemView);
            name_view = (TextView) itemView.findViewById(R.id.name_view);
            dates_view = (TextView) itemView.findViewById(R.id.dates_view);
           name_view.setMinimumWidth(MainActivity.height*25/100);
//            name_view.setMaxWidth(MainActivity.height*25/100);


        }
    }

    public AllNamesAdapter(List<Imiona> listOfNames) {
        this.listOfNames = listOfNames;
    }

    public List<Imiona> getListOfNames() {
        return listOfNames;
    }

    public void setListOfNames(List<Imiona> listOfNames) {
        this.listOfNames = listOfNames;
    }

public static String replaceChars(String str) {
    str=str.replace(".01"," Styczeń");
    str=str.replace(".02"," Luty");
    str=str.replace(".03"," Marzec");
    str=str.replace(".04"," Kwiecień");
    str=str.replace(".05"," Maj");
    str=str.replace(".06"," Czerwiec");
    str=str.replace(".07"," Lipiec");
    str=str.replace(".08"," Sierpień");
    str=str.replace(".09"," Wrzesień");
    str=str.replace(".10"," Październik");
    str=str.replace(".11"," Listopad");
    str=str.replace(".12"," Grudzień");
    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    return pattern.matcher(nfdNormalizedString).replaceAll("");
}

}
