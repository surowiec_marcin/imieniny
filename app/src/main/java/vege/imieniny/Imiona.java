package vege.imieniny;

/**
 * Created by Vege on 19.08.2017.
 */

public class Imiona {
    private String name;
    private String date;
    private int istotne;

    public Imiona(String name, String date, int istotne) {
        this.name = name;
        this.date = date;
        this.istotne = istotne;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIstotne() {
        return istotne;
    }

    public void setIstotne(int istotne) {
        this.istotne = istotne;
    }
}
